const {connect,StringCodec} = require('nats');
const NATS_SRV = 'nats://nats:4222'; //todo move to env
class Main{

    constructor(){

        this.sc = new StringCodec();
        this.nats = null;
        
    }

    async connect(){

        return this.nats = await connect(NATS_SRV);
    
    }

    async subscribe (topic){

        const sub = this.nats.subscribe(topic);
        (async (sub) => {
            console.log(`listening for ${sub.getSubject()} requests...`);
            for await (const m of sub) {
              if (m.respond({time:new Date().getTime()})) {
                console.info(`[time] handled #${sub.getProcessed()}`);
              } else {
                console.log(`[time] #${sub.getProcessed()} ignored - no reply subject`);
              }
            }
            console.log(`subscription ${sub.getSubject()} drained.`);
          })(sub);
    }

    receive (topic,message) {

        console.log ('receive message',topic,message)

    }

    send(topic,message={body:"Yo! "}){

        console.log('topic',topic)
        this.nats.publish('copier.request',this.sc.encode(message));

    }




}

async function start (){

    const app = new Main();
    const status = await app.connect();
    app.subscribe('copier.command');
    app.send('copier.request');
    console.log('started');

}

start();
