FROM node:current-alpine

ENV NODE_ENV=production

RUN mkdir /app
WORKDIR /app
EXPOSE 4222
COPY package.json package-lock.json ./

RUN npm install --production

COPY . .

CMD ["npm", "start"]
